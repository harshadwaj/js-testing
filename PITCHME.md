
## End to End JS Testing

---

### Requirements for browser testing
<br>
* Consistent way to do multiple testing paradigms
* Supports both BDD and TDD workflows
* Supports tests in JS
* Has high level browser interaction APIs
* Minimal non-JS dependency
* Visual Debugging
---

### Contenders
<br>
* Casper
* Phantom
* Zombie
* Selenium/WebdriverIO
* Nightmare
* Chromeless

---

## Best Option
<br>
### Nightmare


---

## Issues with Casper
<br>
* Casper node module does not work out of box
* Python and Phantom dependency.
* Mocha/Chai not natively supported. Have to use other libs.
* No visual debugging. Have to use screenshots.

---

## Casper Example

```js
var casper = require('casper').create();
casper.start('http://casperjs.org/');

casper.then(function() {
    this.echo('First Page: ' + this.getTitle());
});

casper.thenOpen('http://phantomjs.org', function() {
    this.echo('Second Page: ' + this.getTitle());
});

casper.run();
```

---

## Advantages with Nightmare
<br>
* Based on Electron. 
* Easy integration with node ecosystem.
* TDD, BDD and Fluent Mocha/Chai style native support
* Very rich high level browser interaction APIs
* Niffy - perceptual diffing tool - for regression tests
* Daydream - Chrome extension - macro like to automate Nightmare scripts
* Screenshots, Web Scraping etc.
---


## Nightmare Example

```js
var Nightmare = require('nightmare');   
var nightmare = Nightmare({ show: true });

nightmare
  .goto('https://duckduckgo.com')
  .type('#search_form_input_homepage', 'github nightmare')
  .click('#search_button_homepage')
  .wait('#zero_click_wrapper .c-info__title a')
  .evaluate(function () {
    return document.querySelector('#zero_click_wrapper .c-info__title a').href;
  })
  .end()
  .then(function (result) {
    console.log(result);
  })
  .catch(function (error) {
    console.error('Search failed:', error);
  });

```

---

## Nightmare Integration

```
test_run_js:
  type: sbb-test-run-js
  files:
    - Tests/example1.js
    - Tests/example2.js
```

---

## Things to watch out
<br>
* Headless Chrome from v60+
* Chromeless

---

## Closing Notes
<br>
* Unit testing => Mocha/Chai
* Browser testing => Nightmare




















